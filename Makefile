init:
	docker-compose run --rm greed npm install

spec:
	docker-compose run --rm greed npm test

down:
	docker-compose down

run:
	docker-compose run --rm greed $(args)
