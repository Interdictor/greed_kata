const expect = require('chai').expect
const Greed = require('../src/greed')

describe('Greed', () => {
  context('given a failed rolls combination', () => {
    xit('scores 0 points', () => {
      const failedRolls = [2, 2, 3, 4, 6]
      const greed = new Greed()

      const scoreResult = greed.score(failedRolls)

      expect(scoreResult).equal(0)
    })
  })

  context('given rolls with only one "1"', () => {
    xit('scores 100 points', () => {
      const rolls = [1, 2, 3, 4, 6]
      const greed = new Greed()

      const scoreResult = greed.score(rolls)

      expect(scoreResult).equal(100)
    })
  })

  context('given rolls with only one "5"', () => {
    xit('scores 50 points', () => {
      const rolls = [2, 3, 4, 5, 6]
      const greed = new Greed()

      const scoreResult = greed.score(rolls)

      expect(scoreResult).equal(50)
    })
  })

  context('given rolls with a regular triple', () => {
    xit('scores properly', () => {
      const rollsWithTriple = [2, 2, 2, 3, 4]
      const greed = new Greed()

      const scoreResult = greed.score(rollsWithTriple)

      expect(scoreResult).equal(200)
    })
  })

  context('given rolls with a triple one', () => {
    xit('scores 1000 points', () => {
      const rollsWithTriple = [1, 1, 1, 3, 4]
      const greed = new Greed()

      const scoreResult = greed.score(rollsWithTriple)

      expect(scoreResult).equal(1000)
    })
  })

  context('given rolls with fives and ones combination', () => {
    xit('scores properly', () => {
      const rollsWithTriple = [1, 5, 5, 5, 1]
      const greed = new Greed()

      const scoreResult = greed.score(rollsWithTriple)

      expect(scoreResult).equal(700)
    })
  })

  context('given complex rolls', () => {
    xit('scores properly', () => {
      const rollsWithTriple = [6, 6, 6, 4, 5]
      const greed = new Greed()

      const scoreResult = greed.score(rollsWithTriple)

      expect(scoreResult).equal(650)
    })
  })
})
